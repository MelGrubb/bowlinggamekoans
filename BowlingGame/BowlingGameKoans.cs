﻿using System.Linq;
using NUnit.Framework;

namespace BowlingGame
{
    [TestFixture]
    public class BowlingGameKoans
    {
        [Test]
        public void Koans()
        {
            Assert.AreEqual(0, Game.Score(), "A frameless game should score 0.");

            var gutterGame = Enumerable.Range(1, 10).Select(x => new Frame(0, 0)).ToArray();
            Assert.AreEqual(0, Game.Score(gutterGame), "A gutter game should score 0.");

            var onesGame = Enumerable.Range(1, 10).Select(x => new Frame(1, 1)).ToArray();
            Assert.AreEqual(20, Game.Score(onesGame), "An all-ones game should score 20.");

            var spare = new[] {new Frame(5, 5), new Frame(3, 0)};
            Assert.AreEqual(16, Game.Score(spare), "The score for a spare should include the next roll as a bonus.");

            var strike = new[] { new Frame(10, 0), new Frame(3, 4) };
            Assert.AreEqual(24, Game.Score(strike), "The score for a strike should include the next two rolls as a bonus.");

            var perfectGame = Enumerable.Range(1, 9).Select(x => new Frame(10, 0)).Concat(new[] {new Frame(10, 10, 10)}).ToArray();
            Assert.AreEqual(300, Game.Score(perfectGame), "A perfect game should score 300");
        }
    }
}