﻿My approach this time is a little different than the way this kata is normally written, so I wanted
to explain. With a kata, the goal is normally to follow a pre-determined set of "moves" so that you 
might absorb them and learn the techniques they demonstrate. I have, of course, performed Uncle Bob's 
bowling kata in its original form, but since this time the goal is to serve as a coding exercise and
example rather than a traditional kata, I wanted to do things a little differently so that you might 
see my own individual approach to the problem.

This is what I came up with when I skipped the "Quick Design Session" and started from scratch.
My Game class is static and stateless, so that rather than accumulating rolls over the course of the 
game, it scores the entire game up to the current point whenever it is called. Rather than taking a 
series of individual "Rolls", it takes an array of "Frames", which seemed like a logical way to break 
down the game.

The standard five tests from the original kata are still represented, adapted for the way in which my 
Game class is written, of course.


Kata vs. Koan
For simpler katas, I like to write all of the tests out ahead of time in a "Koans" type format where
the only way to get to the next test is to pass the current one. This is different than writing the 
tests out one at a time as the kata progresses, but I have found it to be an effective teaching tool.
Ordinarily, I would give this finished test class to a student at the beginning of the exercise, 
but tell them not to "read ahead" by looking at the tests. Instead, they should just do what the tests
tell them to, one step at a time. It's totally at odds with the purist "one assert per test" approach, 
since all of the assertions are together in one single test.

I thought I would present this kata in this format simply because it is an interesting variation on 
the regular kata approach, and might spark some conversation. It's certainly not traditional, but I 
don't find it offensively sacrilegious either.

Not all katas can be represented in this format. One notable example where this doesn't work is Guy 
Royse's EverCraft kata. We attempted to "koan-ify" Evercraft a few years ago and it didn't work out well. 
The tests were largely reflection-based so that the student did not have to stub out everything just to 
get the first compile to happen, but the tests were still very specific about what class and property 
names they were expecting. As a Kata, this would normally be fine since you should be performing the same 
"moves" each time, so perhaps I'll revisit the Evercraft "Koan" at some point in the future.