using System.Linq;

namespace BowlingGame
{
    public class Frame
    {
        public Frame(params int[] rolls)
        {
            Rolls = rolls;
        }

        public bool IsSpare => Score == 10;
        public bool IsStrike => Rolls[0] == 10;
        public int[] Rolls { get; set; }
        public int Score => Rolls.Sum();
        public int SpareBonus => Rolls[0];
        public int StrikeBonus => Rolls[1];
    }
}