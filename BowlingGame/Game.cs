namespace BowlingGame
{
    public static class Game
    {
        public static int Score(params Frame[] frames)
        {
            var score = 0;

            for (var index = 0; index < frames.Length; index++)
            {
                var frame = frames[index];
                var nextFrame = frames.Length > index + 1 ? frames[index + 1] : null;
                score += frame.Score;

                if (!frame.IsSpare || nextFrame == null) continue;

                score += nextFrame.SpareBonus;

                if (!frame.IsStrike) continue;

                var nextNextFrame = frames.Length > index + 2 ? frames[index + 2] : null;
                score += nextFrame.IsStrike && nextNextFrame != null ? nextNextFrame.SpareBonus : nextFrame.StrikeBonus;
            }

            return score;
        }
    }
}